<?php

// [SECTION] Comments

/*
    There are two types of comments:
    - single-line comment denoted with two forward slashes (ctrl + /)
    - multiline comment denoted by a forward slash and asterisk (alt + shift + /)
*/


// [SECTION] Variables

    // Variables are defined using the dollar ($) notation before the variable name.

    $name = "John Smith";
    $email = "johnsmith@gmail.com";


// [SECTION] Constants
/* 
    - Constants are defined using the define() function
    - Naming convention for "constant" variables should be in ALL CAPS.
    - single or double quotes work in PHP
*/
    // Does not use the $ notation before the variable.
    define('PI', 3.1416);

// Variable vs Constant
    // Constants do not follow any variable scoping.
    // Variables can be declared anywhere in the program, but they follow variable scoping rules.


// [SECTION] Echoing Values
    // (you will see in index.php file)


// [SECTION] Data Types

    // String
    $state = 'New York';
    $country = 'United States of America';

    // Single quote concatenation via . sign
    $address = $state . ', ' . $country;

    // Concatenation via double quote
    $address = "$state, $country";

    // Integers
    $age = 31;
    $headcount = 26;

    // Floats
    $grade = 98.2;
    $distanceInKilometers = 1342.12;

    // Boolean
    $hasTravelledAbroad = false;
    $haveSymptoms = true;

    // Arrays
    // array() functon is used to declare arrays
    $grades = array(98.7, 92.1, 90.2, 94.6);

    // Null
    // absence of a value or indicating that something has no value
    $spouse = null;
    $middleName = null;

    // Objects
    // using path arrow
    $gradesObj = (object)[
        'firstGrading' => 98.7,
        'secondGrading' => 92.1,
        'thirdGrading' => 90.2,
        'fourthGrading' => 94.6
    ];

    $personObj = (object)[
        'fullName' => 'John Smith',
        'isMarried' => false,
        'age' => 35,
        'address' => (object)[
            'state' => 'New York',
            'country' => 'United States of America'
        ]
    ];


// [SECTION] Operators

    // Assignment Operator (=)
    $x = 250;
    $y = 120;

    $isLegalAge = true;
    $isRegistered = false;


// [SECTION] Functions
// Functions are used to make reusable code.

    function getFullName($firstName, $middleInitial, $lastName){
        return "$lastName, $firstName, $middleInitial";
    }


// [SECTION] Selection Control Structures

    // if-elseif-else Statement
    function determineTyphoonIntensity($windSpeed) {
        if($windSpeed < 30) {
            return 'Not a typhoon yet.';
        }
        else if($windSpeed <= 61) {
            return 'Tropical depression detected.';
        }
        else if($windSpeed >= 62 && $windSpeed <= 88) {
            return 'Tropical storm detected.';
        }
        else if($windSpeed >= 89 && $windSpeed <= 117) {
            return 'Severe Tropical storm detected.';
        }
        else {
            return 'Typhoon detected.';
        }
    }

    // Conditional (Ternary) Operator
    function isUnderAge($age) {
        return ($age < 18) ? true : false;
    }

    // Switch Statement
	function determineComputerUser($computerNumber) {
	    switch ($computerNumber) {
	        case 1: 
	            return 'Linus Torvalds';
	            break;
	        case 2:
	            return 'Steve Jobs';
	            break;
	        case 3:
	            return 'Sid Meier';
	            break;
	        case 4:
	            return 'Onel de Guzman';
	            break;
	        case 5:
	            return 'Christian Salvador';
	            break;
            default:
	            return $computerNumber.' is out of bounds.';
	            break;
	    }
	}

    // try-catch-finally Statement
    function greeting($str) {
        try{
            if(gettype($str) === "string") {
                echo $str;
            }
            else {
                throw new Exception("Oops!");
            }
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
        finally{
            echo " I did it again!";
        }
    }