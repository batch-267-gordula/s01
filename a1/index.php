<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity Code 01</title>
</head>
<body>

<?php require_once "./code.php"; ?>

<h1>Full Address</h1>
<p><?php echo getFullAddress("Philippines", "Metro Manila", "Caloocan City", "Block 1, Lot 1, Natividad Subdivision"); ?></p>
<p><?php echo getFullAddress("Thailand", "Bangkok", "Bangkok City", "Issi Condo, Room 735"); ?></p>

    
<h1>Letter-Based Grading</h1>
<p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
<p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
<p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>

</body>
</html>